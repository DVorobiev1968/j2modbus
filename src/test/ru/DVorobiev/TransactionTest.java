package ru.DVorobiev;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;
import com.ghgande.j2mod.modbus.io.AbstractModbusTransport;
import com.ghgande.j2mod.modbus.io.ModbusTransaction;
import com.ghgande.j2mod.modbus.msg.*;
import com.ghgande.j2mod.modbus.procimg.InputRegister;
import com.ghgande.j2mod.modbus.procimg.Register;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import ru.DVorobiev.io.DataOutputForRegister;
import ru.DVorobiev.io.WorkRegister;
import ru.DVorobiev.report.DataRegister;
import ru.DVorobiev.report.ReportExcel;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Random;

@Slf4j
public class TransactionTest {
    private float fValue;
    private String errMessage;
    private static int transactionId=1;
    private String strReport;

    private void readCommEventCounter(int idNodeSlave){
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ModbusTransaction transaction=null;
        AbstractModbusTransport transport=null;
        String errMessage = new String();
        try {
            master.connect();
            transport=master.getTransport();
            transport.setTimeout(10);
            ModbusRequest request=new ReadCommEventCounterRequest();
            request.setUnitID(idNodeSlave);
            request.setHeadless(transaction instanceof ModbusTransaction);
            request.setTransactionID(this.transactionId++);
            errMessage=String.format("Request: %s",request.getHexMessage());
            log.debug(errMessage);
            transaction=transport.createTransaction();
            transaction.setRequest(request);
            transaction.execute();
            ModbusResponse response=transaction.getResponse();
            errMessage=String.format("Response: %s",response.getHexMessage());
            log.debug(errMessage);
            ReadCommEventCounterResponse data = (ReadCommEventCounterResponse)response;
            errMessage=String.format("Status: %d, Events: %d", data.getStatus(), data.getEventCount());
            log.info(errMessage);
            transport.close();
        } catch (ModbusException e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }
    private void readWriteInSession(ModbusTCPMaster master, int idNodeSlave, int readRef, int readCount, int writeRef, int writeCont, float fValue) {
        ModbusTransaction transaction = null;
        AbstractModbusTransport transport = null;
        this.errMessage = new String();
        try {
            transport = master.getTransport();
            transport.setTimeout(10);
            ReadWriteMultipleRequest request = new ReadWriteMultipleRequest(idNodeSlave, readRef, readCount, writeRef, writeCont);
            request.setHeadless(transaction instanceof ModbusTransaction);
            request.setTransactionID(this.transactionId++);
            WorkRegister registers=new WorkRegister();
            request.setRegisters(registers.floatToRegister(fValue));
            errMessage = String.format("Write fValue: %4.15f;Request: %s", fValue,request.getHexMessage());
            log.debug(errMessage);
            transaction = transport.createTransaction();
            transaction.setRequest(request);
            transaction.execute();
            ModbusResponse response = transaction.getResponse();
            errMessage = String.format("Response: %s", response.getHexMessage());
            log.debug(errMessage);
            errMessage = String.format("[");
            ReadWriteMultipleResponse data = (ReadWriteMultipleResponse) response;
            InputRegister[] inputRegisters = new InputRegister[TypesData.SIZE_32];
            for (int i = 0; i < data.getWordCount(); i++) {
                inputRegisters[i] = data.getRegister(i);
                errMessage += String.format("%d,", inputRegisters[i].getValue());
            }
            errMessage = errMessage.substring(0, errMessage.length() - 1);
            errMessage += String.format("]");
            WorkRegister workRegister = new WorkRegister();
            this.fValue=workRegister.registerToFloat(inputRegisters);
            String sValue = String.format("%4.15f;", this.fValue);
            log.debug(sValue + errMessage);
            transport.close();
        } catch (ModbusException | IOException e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        }
    }
    public void readWriteRequest(int idNodeSlave, int readRef, int readCount, int writeRef, int writeCont){
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ModbusTransaction transaction=null;
        AbstractModbusTransport transport=null;
        String errMessage = new String();
        try {
            master.connect();
            final Random random=new Random();
            this.fValue=random.nextFloat();
            readWriteInSession(master,idNodeSlave,readRef,readCount,writeRef,writeCont,fValue);
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }
    private void readWriteCycle(int idNodeSlave, int readRef, int readCount, int writeRef, int writeCont,int timeout, int itteration){
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ReportExcel reportExcel = new ReportExcel(String.format("readWriteCycleRegisters_%d_IDSlave",idNodeSlave));
        ModbusTransaction transaction=null;
        AbstractModbusTransport transport=null;
        String errMessage = new String();
        try {
            master.connect();
            while (itteration-->0) {
                long currentTimeMillis = System.currentTimeMillis();
                final Random random=new Random();
                this.fValue= random.nextFloat();
                strReport=String.format("%4.15f",this.fValue);
                readWriteInSession(master, idNodeSlave, readRef, readCount, writeRef, writeCont,this.fValue);
                DataRegister dataRegister=new DataRegister(readRef,currentTimeMillis,(double)this.fValue,strReport);
                reportExcel.list.add(dataRegister);
            }
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
                reportExcel.CreateReport();
            }
        }
    }
    @Ignore
    @Test
    public void readCommEventCounterTest(){
        readCommEventCounter(1);
    }
    @Test
    public void readWriteRequiestTest(){
        readWriteRequest(1,1,TypesData.SIZE_32,1,TypesData.SIZE_32);
    }
    @Test
    public void readWriteCycleTest(){
        long start = System.currentTimeMillis();
        String errMessage = "Starting readWriteCycleTest...";
        log.info(errMessage);
        readWriteCycle(1,3,TypesData.SIZE_32,1,TypesData.SIZE_32,200,10);
        long time = System.currentTimeMillis() - start;
        errMessage = String.format("Test readWriteCycleTest time: %d(ms)", time);
        log.info(errMessage);
    }
}
