package ru.DVorobiev;

import lombok.experimental.UtilityClass;

import java.util.HashMap;

@UtilityClass
public class TypeThreadMethod {
    public static final int WRITE_CYCLE_REGISTER=1;
    public static final int READ_CYCLE_REGISTER=2;
    private static final HashMap<Integer, String> STATUSES = new HashMap<>();
    static {
        STATUSES.put(WRITE_CYCLE_REGISTER,"writeCycleRegisterTest");
        STATUSES.put(READ_CYCLE_REGISTER,"readCycleRegisterTest");
    }
    public String methodMessage(int codeErr) {
        String methodMessage;
        methodMessage = STATUSES.get(codeErr);
        return methodMessage;
    }
}
