package ru.DVorobiev;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;
import com.ghgande.j2mod.modbus.io.AbstractModbusTransport;
import com.ghgande.j2mod.modbus.io.ModbusTransaction;
import com.ghgande.j2mod.modbus.msg.*;
import com.ghgande.j2mod.modbus.procimg.Register;
import com.ghgande.j2mod.modbus.util.ModbusUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import ru.DVorobiev.io.WorkRegister;
import ru.DVorobiev.report.DataRegister;
import ru.DVorobiev.report.ReportExcel;
import ru.DVorobiev.utils.Utils;

import java.util.Random;

@Slf4j
public class MasterTest {
    private void readRegisters(int idNodeSlave, int address) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        String str = new String();
        String errMessage = new String();
        try {
            master.connect();
            long currentTimeMillis = System.currentTimeMillis();
            Register[] registers = master.readMultipleRegisters(idNodeSlave, address, TypesData.SIZE_32);
            byte[] appBytes = new byte[0];
            String strReport=new String("[");
            for (Register register : registers) {
                strReport+=String.format("%d,", register.getValue());
                byte[] bytes = register.toBytes();
                appBytes = Utils.concatenate(appBytes, bytes);
            }
            strReport=strReport.substring(0,strReport.length()-1);
            strReport += String.format("]");
            appBytes=Utils.reverseByteArray(appBytes);
            float float32Bit = ModbusUtil.registersToFloat(appBytes);
            DataRegister dataRegister=new DataRegister(address,currentTimeMillis,(double)float32Bit,strReport);
            log.info(dataRegister.toString());
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }
    private void readCycleRegisters(int idNodeSlave, int address, int timeout, int itteration) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ReportExcel reportExcel = new ReportExcel(String.format("readCycleRegisters_%d_IDSlave",idNodeSlave));
        String str = new String();
        String errMessage = new String();
        try {
            master.connect();
            while (itteration-->0) {
                long currentTimeMillis = System.currentTimeMillis();
                Register[] registers = master.readMultipleRegisters(idNodeSlave, address, TypesData.SIZE_32);
                byte[] appBytes = new byte[0];
                String strReport = new String("[");
                for (Register register : registers) {
                    strReport += String.format("%d,", register.getValue());
                    byte[] bytes = register.toBytes();
                    appBytes = Utils.concatenate(appBytes, bytes);
                }
                strReport = strReport.substring(0, strReport.length() - 1);
                strReport += String.format("]");
                appBytes = Utils.reverseByteArray(appBytes);
                float float32Bit = ModbusUtil.registersToFloat(appBytes);
                DataRegister dataRegister=new DataRegister(address,currentTimeMillis,(double)float32Bit,strReport);
                reportExcel.list.add(dataRegister);
                log.debug(dataRegister.toString());
                Thread.sleep(timeout);
            }
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        reportExcel.CreateReport();
    }

    private void readWriteCycleRegisters(int idNodeSlave, int address, int timeout, int itteration) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ReportExcel reportExcel = new ReportExcel(String.format("readCycleRegisters_%d_IDSlave",idNodeSlave));
        String str = new String();
        String errMessage = new String();
        try {
            master.connect();
            while (itteration-->0) {
                long currentTimeMillis = System.currentTimeMillis();
                Register[] registers = master.readMultipleRegisters(idNodeSlave, address, TypesData.SIZE_32);
                byte[] appBytes = new byte[0];
                String strReport = new String("[");
                for (Register register : registers) {
                    strReport += String.format("%d,", register.getValue());
                    byte[] bytes = register.toBytes();
                    appBytes = Utils.concatenate(appBytes, bytes);
                }
                strReport = strReport.substring(0, strReport.length() - 1);
                strReport += String.format("]");
                appBytes = Utils.reverseByteArray(appBytes);
                float float32Bit = ModbusUtil.registersToFloat(appBytes);
                DataRegister dataRegister=new DataRegister(address,currentTimeMillis,(double)float32Bit,strReport);
                reportExcel.list.add(dataRegister);
                log.debug(dataRegister.toString());
                Thread.sleep(timeout);
            }
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        reportExcel.CreateReport();
    }

    private void readMultiRegisters(int idNodeSlave) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ReportExcel reportExcel = new ReportExcel(String.format("MasterSlave_%d",idNodeSlave));

        String str = new String();
        String errMessage = new String();
        try {
            master.connect();
            for (int i = 0; i < TypesData.MAX_COUNT_REGISTER; i += TypesData.SIZE_32) {
                long currentTimeMillis = System.currentTimeMillis();
                Register[] registers = master.readMultipleRegisters(idNodeSlave, i+1, TypesData.SIZE_32);
                byte[] appBytes = new byte[0];
                String strReport=new String("[");
                for (Register register : registers) {
                    strReport+=String.format("%d,", register.getValue());
                    byte[] bytes = register.toBytes();
                    appBytes = Utils.concatenate(appBytes, bytes);
                }
                strReport=strReport.substring(0,strReport.length()-1);
                strReport += String.format("]");
                appBytes=Utils.reverseByteArray(appBytes);
                float float32Bit = ModbusUtil.registersToFloat(appBytes);
                DataRegister dataRegister=new DataRegister(i,currentTimeMillis,(double)float32Bit,strReport);
                reportExcel.list.add(dataRegister);
                str += String.format("%s,float32:%4.10f\n", strReport,float32Bit);
            }
            log.info(str);
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        reportExcel.CreateReport();
    }

    private float calc(float value){
        return 0-value;
    }

    private void writeRegistersInSession(ModbusTCPMaster master,int idNodeSlave, int address, float value){
        try {
            byte[] bytes=ModbusUtil.floatToRegisters(value);
            bytes=Utils.reverseByteArray(bytes);
            Register[] registers=new Register[TypesData.SIZE_32];
            int index=0;
            for (int iByte=0; iByte<TypesData.SIZE_WORD; iByte+=2){
                registers[index]=new WorkRegister(bytes[index*iByte],bytes[index*iByte+1]);
                index++;
            }
            master.writeMultipleRegisters(idNodeSlave,address,registers);

        } catch (ModbusException e) {
            e.printStackTrace();
        }
    }
    /** Метод записывает значение в регистр во внешний узел в указанный адрес */
    private void writeRegisters(int idNodeSlave, int address) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);

        String errMessage = new String();
        final Random random=new Random();
        try {
            master.connect();
            long currentTimeMillis = System.currentTimeMillis();
            Float float32Bit= random.nextFloat();
            byte[] bytes=ModbusUtil.floatToRegisters(float32Bit);
            bytes=Utils.reverseByteArray(bytes);
            Register[] registers=new Register[TypesData.SIZE_32];
            String strRegister = new String("[");
            int index=0;
            for (int iByte=0; iByte<TypesData.SIZE_WORD; iByte+=2){
                registers[index]=new WorkRegister(bytes[index*iByte],bytes[index*iByte+1]);
                strRegister+=String.format("%s,",registers[index].toString());
                index++;
            }
            strRegister=strRegister.substring(0,strRegister.length()-1);
            strRegister+=String.format("]");
            master.writeMultipleRegisters(idNodeSlave,address,registers);
            writeRegistersInSession(master,idNodeSlave,address+2,float32Bit);
            DataRegister dataRegister=new DataRegister(address,currentTimeMillis,(double)float32Bit,strRegister);
            log.info(dataRegister.toString());
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }
    /** Метод записывает значение в регистр во внешний узел в указанный адрес */
    private void writeCycleRegistersOld(int idNodeSlave, int address, int timeout, int itteration) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ReportExcel reportExcel = new ReportExcel(String.format("writeCycleRegisters_%d_IDSlave",idNodeSlave));

        String errMessage = new String();
        final Random random=new Random();
        try {
            master.connect();
            while (itteration-->0) {
                long currentTimeMillis = System.currentTimeMillis();
                Float float32Bit = random.nextFloat();
                byte[] bytes = ModbusUtil.floatToRegisters(float32Bit);
                bytes = Utils.reverseByteArray(bytes);
                Register[] registers = new Register[TypesData.SIZE_32];
                String strRegister = new String("[");
                int index = 0;
                for (int iByte = 0; iByte < TypesData.SIZE_WORD; iByte += 2) {
                    registers[index] = new WorkRegister(bytes[index * iByte], bytes[index * iByte + 1]);
                    strRegister += String.format("%s,", registers[index].toString());
                    index++;
                }
                strRegister = strRegister.substring(0, strRegister.length() - 1);
                strRegister += String.format("]");
                master.writeMultipleRegisters(idNodeSlave, address, registers);
                DataRegister dataRegister = new DataRegister(address, currentTimeMillis, (double) float32Bit, strRegister);
                reportExcel.list.add(dataRegister);
                log.info(dataRegister.toString());
                Thread.sleep(timeout);
            }
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        reportExcel.CreateReport();
    }
    /** Метод записывает значение в регистр во внешний узел в указанный адрес */
    private void writeCycleRegisters(int idNodeSlave, int address, int timeout, int itteration) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ReportExcel reportExcel = new ReportExcel(String.format("writeCycleRegisters_%d_IDSlave",idNodeSlave));

        String errMessage = new String();
        final Random random=new Random();
        try {
            master.connect();
            while (itteration-->0) {
                long currentTimeMillis = System.currentTimeMillis();
                Float float32Bit = random.nextFloat();
                writeRegistersInSession(master,idNodeSlave,address,float32Bit);
                DataRegister dataRegister = new DataRegister(address,
                        currentTimeMillis,
                        (double) float32Bit);
                reportExcel.list.add(dataRegister);
                // имитация работы Алгоритма Beremiz, убрать после отладки
                writeRegistersInSession(master,idNodeSlave,address+TypesData.SIZE_32,calc(float32Bit));
                log.debug(dataRegister.toString());
                Thread.sleep(timeout);
            }
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        reportExcel.CreateReport();
    }
    /** Метод читает, затем записывает новое рассчитанное значение в регистр во внешний узел */
    private void writeMultiRegistersRefactoring(int idNodeSlave) {
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        ReportExcel reportExcel = new ReportExcel(String.format("MasterSlave_%d",idNodeSlave));

        String errMessage = new String();
        final Random random=new Random();
        try {
            master.connect();
            for (int i = 0; i < TypesData.MAX_COUNT_REGISTER*2; i += TypesData.SIZE_32) {
                long currentTimeMillis = System.currentTimeMillis();
                Float float32Bit= random.nextFloat();
                WorkRegister registers=new WorkRegister();
                master.writeMultipleRegisters(idNodeSlave,i+1,registers.floatToRegister(float32Bit));
                DataRegister dataRegister=new DataRegister(i,currentTimeMillis,(double)float32Bit,registers.getErrMessage());
                reportExcel.list.add(dataRegister);
            }
            log.debug(errMessage);
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        reportExcel.CreateReport();
    }
    @Test
    public void readRegisterTest(){
        int address = 100;
        SimpleMaster simpleMaster = new SimpleMaster();
        //TODO  используем имитатор Beremiz с SlaveNode=3, адрес 100
        int errno = simpleMaster.readCycleIR(1, address, 1, 10, 2);
        String logs = String.format("%d;%s", errno, simpleMaster.getErrMessage());
        log.info(logs);
    }

    @Ignore
    @Test
    public void readCycleRegisterTest(){
        SimpleMaster simpleMaster = new SimpleMaster();
        int errno = simpleMaster.readCycleHRSync(1,3,1000);
        String logs = String.format("%d;%s", errno, simpleMaster.getErrMessage());
        log.info(logs);
//        readCycleRegisters(1,3,20,1000);
    }

    @Ignore
    @Test
    public void readMultiRegisterTest(){
        readMultiRegisters(1);
    }
    @Ignore
    @Test
    public void writeMultiRegisterTest(){
        writeMultiRegistersRefactoring(1);
    }
    @Ignore
    @Test
    public void writeRegisterTest(){
        SimpleMaster simpleMaster = new SimpleMaster();
        int errno=simpleMaster.writeCycleHR(1,3,200,1000);
        String logs = String.format("%d;%s", errno, simpleMaster.getErrMessage());
        log.info(logs);
    }
    @Ignore
    @Test
    public void writeCycleRegisterTest(){
        SimpleMaster simpleMaster = new SimpleMaster();
        int errno=simpleMaster.writeCycleHRSync(1,1,200,1000);
        String logs = String.format("%d;%s", errno, simpleMaster.getErrMessage());
        log.info(logs);
    }
    @Test
    public void createRegisterTest(){
        final Random random=new Random();
        Float float32Bit= random.nextFloat();
        WorkRegister registers=new WorkRegister();
        registers.floatToRegister(float32Bit);
        log.info(registers.getErrMessage());
    }

    @Ignore
    @Test
    public void readWriteTest() {
        try {
            ThreadTest testThread1 = new ThreadTest("WriteCycleRegisterTest",TypeThreadMethod.WRITE_CYCLE_REGISTER);
            ThreadTest testThread2 = new ThreadTest("ReadCycleRegisterTest",TypeThreadMethod.READ_CYCLE_REGISTER);
            testThread1.start();
            testThread2.start();
            while (testThread1.getStateThread() < ThreadTest.CANCEL_THREAD
                    && testThread2.getStateThread() < ThreadTest.CANCEL_THREAD)
                Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
