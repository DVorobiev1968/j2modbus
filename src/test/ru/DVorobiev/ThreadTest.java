package ru.DVorobiev;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Тестовый пример для распараллеливания потоков, демонстрирует следующие функции:
 *
 * <ol>
 *   <li>инициализация потока
 *   <li>переопределение метода run(), в котором реализуется запрос на сервер технологических данных
 *       для записи значений
 *   <li>const состояния потока, для управления из запускающего приложения
 * </ol>
 */
@Getter
@Setter
@Slf4j
public class ThreadTest implements Runnable {
    public static final int INIT_THREAD = 0;
    public static final int START_THREAD = 1;
    public static final int ERROR_THREAD = -1;
    public static final int RUN_THREAD = 2;
    public static final int CANCEL_THREAD = 3;

    private final String nameThread;
    private final int typeThread;
    private String errMessage;
    private int stateThread;

    Thread threadTest;

    ThreadTest(String name, int typeThread) {
        this.typeThread = typeThread;
        stateThread = INIT_THREAD;
        nameThread = name;
    }

    @Override
    public void run() {
        try {
            int countErr = 0;
            this.stateThread = RUN_THREAD;
            long start = System.currentTimeMillis();
            errMessage = String.format("Thread %s running...", nameThread);
            log.info(errMessage);
            MasterTest masterTest=new MasterTest();
            switch (this.typeThread){
                case TypeThreadMethod.WRITE_CYCLE_REGISTER:
                    masterTest.writeCycleRegisterTest();
                case TypeThreadMethod.READ_CYCLE_REGISTER:
                    masterTest.readCycleRegisterTest();
            }
            long time = System.currentTimeMillis() - start;
            float ms = (float) (time / 1000);
            errMessage =
                    String.format(
                            "Thread %s:Test writeCycleRegisterTest time: %4.3f(sec.)",
                            nameThread, ms);
            log.info(errMessage);
            Thread.sleep(100);
            this.stateThread = CANCEL_THREAD;
        } catch (InterruptedException e) {
            errMessage = "Error:" + e.getMessage();
            log.error(errMessage);
            this.stateThread = ERROR_THREAD;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        errMessage = String.format("Thread %s started...", nameThread);
        if (threadTest == null) {
            this.stateThread = START_THREAD;
            threadTest = new Thread(this, nameThread);
            errMessage = String.format("Thread %s new started!", nameThread);
            threadTest.start();
        }
        log.info(errMessage);
    }

    public String getName() {
        return threadTest.getName();
    }

    public void setPriority(int i) {
        threadTest.setPriority(i);
    }

    public int getPriority() {
        return threadTest.getPriority();
    }
}
