package ru.DVorobiev;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;
import com.ghgande.j2mod.modbus.io.AbstractModbusTransport;
import com.ghgande.j2mod.modbus.io.ModbusTransaction;
import com.ghgande.j2mod.modbus.msg.*;
import com.ghgande.j2mod.modbus.procimg.InputRegister;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import ru.DVorobiev.io.WorkRegister;

import java.util.Arrays;

@Slf4j
public class MasterInputRegisterTest {
    private int iValue;
    private String errMessage;
    private static int transactionId=1;
    private String strReport;

    private void readInSession(ModbusTCPMaster master, int idSlave, int readRef, int readCount) {
        ModbusTransaction transaction = null;
        AbstractModbusTransport transport = null;
        this.errMessage = new String();
        try {
            transport = master.getTransport();
            transport.setTimeout(10);
            ReadInputRegistersRequest request=new ReadInputRegistersRequest(readRef,readCount);
            request.setHeadless(transaction instanceof ModbusTransaction);
            request.setTransactionID(this.transactionId++);
            request.setUnitID(idSlave);
            transaction = transport.createTransaction();
            transaction.setRequest(request);
            transaction.execute();
            ReadInputRegistersResponse response =(ReadInputRegistersResponse) transaction.getResponse();
            InputRegister values[]=response.getRegisters();
            errMessage = String.format("Response: %s", Arrays.toString(values));
            log.debug(errMessage);
        } catch (ModbusException e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        }
    }
    private void readIRInSession(ModbusTCPMaster master, int idSlave, int readRef, int readCount) {
        this.errMessage = new String();
        try {
            InputRegister[] registersOut = master.readInputRegisters(idSlave, readRef,readCount);
            errMessage=registersOut.toString();
            WorkRegister registers=new WorkRegister();
            short iValue=registers.registerToShort(registersOut);
            errMessage = String.format("iValue: %d", iValue);
            log.info(errMessage);
        } catch (ModbusException e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        }
    }

    private short readInSessionRefactor(ModbusTCPMaster master, int idSlave, int readRef, int readCount) {
        this.errMessage = new String();
        short sValue=0;
        try {
            InputRegister[] registersOut = master.readMultipleRegisters(idSlave, readRef,readCount);
            errMessage=registersOut.toString();
            WorkRegister registers=new WorkRegister();
            sValue=registers.registerToShort(registersOut);
            errMessage = String.format("iValue: %d", sValue);
            log.info(errMessage);
        } catch (ModbusException e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        }
        return sValue;
    }
    public void readRegistr(int idNodeSlave, int readRef, int readCount){
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        String errMessage = new String();
        try {
            master.connect();
            readInSessionRefactor(master,idNodeSlave,readRef,readCount);
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }
    public void readRegistrCycle(int idNodeSlave, int readRef, int readCount){
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        String errMessage = new String();
        try {
            master.connect();
            readInSessionRefactor(master,idNodeSlave,readRef,readCount);
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }
    private void addIRCounter(ModbusTCPMaster master, int idSlave, int writeRef) {
        this.errMessage = new String();
        try {
            InputRegister[] registersOut = master.readInputRegisters(idSlave, writeRef,TypesData.SIZE_16);
            WorkRegister registers=new WorkRegister();
            short sValue=registers.registerToShort(registersOut);
            sValue++;
            master.writeSingleRegister(idSlave,writeRef,registers.shortToRegister(sValue));
            errMessage = String.format("sValue: %d", sValue);
            log.info(errMessage);
        } catch (ModbusException e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        }
    }
    private short addHRCounter(ModbusTCPMaster master, int idSlave, int writeRef) {
        this.errMessage = new String();
        short sValue=0;
        try {
            InputRegister[] registersOut = master.readMultipleRegisters(idSlave, writeRef,TypesData.SIZE_16);
            WorkRegister registers=new WorkRegister();
            sValue=registers.registerToShort(registersOut);
            sValue++;
            master.writeSingleRegister(idSlave,writeRef,registers.shortToRegister(sValue));
            errMessage = String.format("sValue: %d", sValue);
            log.info(errMessage);
        } catch (ModbusException e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        }
        return sValue;
    }
    public void readWriteCounter(int idNodeSlave, int readRef){
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        String errMessage = new String();
        try {
            master.connect();
            addHRCounter(master,idNodeSlave,readRef);
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }

    public void writeHRWithCounter(int idNodeSlave, int readRef){
        ModbusTCPMaster master = new ModbusTCPMaster("localhost", 5020);
        String errMessage = new String();
        short readCounter=0;
        try {
            master.connect();

            readCounter=addHRCounter(master,idNodeSlave,readRef);
        } catch (Exception e) {
            errMessage += String.format("Error:%s", e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
    }

    @Ignore
    @Test
    public void readWriteTest(){
        // используется Slave-узел 3, регистр 1, имитация Beremiz, убрать после отладки
        readWriteCounter(3,TypesData.WRITE_COUNTER);
        readRegistr(3,TypesData.WRITE_COUNTER,TypesData.SIZE_16);
//        readInputRegistr(3,TypesData.WRITE_COUNTER,TypesData.SIZE_16);
//        readInputRegistr(3,2,2);
//        readInputRegistr(1,101,2);
//        readInputRegistr(1,100,2);
    }
}
