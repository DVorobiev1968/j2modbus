package ru.DVorobiev;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.facade.ModbusTCPMaster;
import com.ghgande.j2mod.modbus.procimg.InputRegister;
import com.ghgande.j2mod.modbus.procimg.Register;
import com.ghgande.j2mod.modbus.util.ModbusUtil;
import java.util.Random;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.DVorobiev.io.WorkRegister;
import ru.DVorobiev.report.DataRegister;
import ru.DVorobiev.report.ReportExcel;
import ru.DVorobiev.utils.Utils;

@Setter
@Getter
@Slf4j
public class SimpleMaster {
    /** таймаут на запрос информации от Slave-узла */
    private int timeout;
    /** Блок синхронизации Алгоритма */
    private int timeoutSync;
    private long sleepSync;
    /** адрес счетчика на чтение расчетных данных */
    private int readCountAddress;
    /** текущий счетчик на чтение расчетных данных */
    private int checkReadCounter;
    /** адрес счетчика на запись расчетных данных */
    private int writeCountAddress;
    /** текущий счетчик на запись расчетных данных от Алгоритма */
    private int checkWriteCounter;
    /** объект master-узла */
    private ModbusTCPMaster master;
    /** сообщение об ошибке */
    private String errMessage;
    /** код ошибки */
    private int errno;

    public SimpleMaster() {
        this.timeout = 10;
        this.checkReadCounter = 0;
        this.checkWriteCounter = 0;
        this.readCountAddress=100;
        this.writeCountAddress=101;
        this.timeoutSync=Settings.TIMEOUT_READ_COUNTER_DEFAULT;
        this.sleepSync=Settings.SLEEP_READ_COUNTER_DEFAULT;
        this.master = new ModbusTCPMaster(Settings.HOST, Settings.PORT);
        this.errno = ErrorCode.OK;
    }

    public SimpleMaster(int timeout, int readCountAddress, int writeCountAddress) {
        this.timeout = timeout;
        this.checkReadCounter = 0;
        this.checkWriteCounter = 0;
        this.readCountAddress= readCountAddress;
        this.writeCountAddress = writeCountAddress;
        this.timeoutSync=Settings.TIMEOUT_READ_COUNTER_DEFAULT;
        this.sleepSync=Settings.SLEEP_READ_COUNTER_DEFAULT;
        this.master = new ModbusTCPMaster(Settings.HOST, Settings.PORT);
        this.errno = ErrorCode.OK;
    }
    /**
     * Запись значения в указанный регистр внутри сессии
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param value : значение float
     */
    private void writeHRInSession(int idNodeSlave, int address, float value) {
        try {
            byte[] bytes = ModbusUtil.floatToRegisters(value);
            bytes = Utils.reverseByteArray(bytes);
            Register[] registers = new Register[TypesData.SIZE_32];
            int index = 0;
            for (int iByte = 0; iByte < TypesData.SIZE_WORD; iByte += 2) {
                registers[index] = new WorkRegister(bytes[index * iByte], bytes[index * iByte + 1]);
                index++;
            }
            this.master.writeMultipleRegisters(idNodeSlave, address, registers);
        } catch (ModbusException e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(this.errMessage);
        }
    }
    /**
     * Синхронная запись значения в указанный регистр внутри сессии, по следующему алгоритму:
     * <ol>
     *     <li>Читаем счетчик на чтение расчетного значения Алгоритма (checkReadCounter)</li>
     *     <li>Сравниваем с предыдущим (checkReadCounter), c текущим</li>
     *     <li>Если текущее значение READ_COUNTER > checkReadCounter, пишем новое значение</li>
     *     <li>Если текущее значение READ_COUNTER == checkReadCounter, ждем timeout (ms)</li>
     *     <li>Задержака (sleep) в (ms) внутри цикла ожидания timeout </li>
     * </ol>
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param timeout : ожидание появления нового значение READ_COUNTER (ms)
     * @param sleep : Задержака (sleep) в (ms) внутри цикла ожидания timeout
     * @param value : значение float
     */
    private void writeHRInSessionSync(int idNodeSlave, int address, int timeout, long sleep, float value) {
        try {
            byte[] bytes = ModbusUtil.floatToRegisters(value);
            bytes = Utils.reverseByteArray(bytes);
            Register[] registers = new Register[TypesData.SIZE_32];
            int index = 0;
            for (int iByte = 0; iByte < TypesData.SIZE_WORD; iByte += 2) {
                registers[index] = new WorkRegister(bytes[index * iByte], bytes[index * iByte + 1]);
                index++;
            }
            short countRead=readIRInSession(idNodeSlave,this.readCountAddress,TypesData.SIZE_16);
            long currentTimeMillis = System.currentTimeMillis();
            while (countRead==this.checkReadCounter){
                Thread.sleep(sleep);
                countRead=readIRInSession(idNodeSlave,this.readCountAddress,TypesData.SIZE_16);
                if (System.currentTimeMillis()-currentTimeMillis > timeout)
                    break;
            }
            this.checkReadCounter=countRead;
            this.master.writeMultipleRegisters(idNodeSlave, address, registers);
        } catch (ModbusException | InterruptedException e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(this.errMessage);
        }
    }

    // TODO имитация работы Алгоритма Beremiz, убрать после отладки
    private float calc(float value) {
        return 0 - value;
    }

    /**
     * Метод записывает значение в регистр во внешний узел в указанный адрес. Метод используется в
     * целях отладки.
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param timeout : задержка по времени между итерациями по записи значений в регистры
     * @param itteration : кол-во итераций
     * @return errno : код ошибки
     */
    public int writeCycleHR(int idNodeSlave, int address, int timeout, int itteration) {
        ReportExcel reportExcel =
                new ReportExcel(String.format("writeCycleHR_%d_IDSlave", idNodeSlave));
        final Random random = new Random();
        try {
            this.master.connect();
            while (itteration-- > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                float float32Bit = random.nextFloat();
                writeHRInSession(idNodeSlave, address, float32Bit);
                DataRegister dataRegister =
                        new DataRegister(address, currentTimeMillis, (double) float32Bit);
                reportExcel.list.add(dataRegister);
                // TODO имитация работы Алгоритма Beremiz, убрать после отладки
                // writeRegistersInSession(master,idNodeSlave,address+TypesData.SIZE_32,calc(float32Bit));
                log.debug(dataRegister.toString());
                Thread.sleep(timeout);
            }
            this.errno = reportExcel.CreateReport();
            this.errMessage = reportExcel.getErrMessage();
        } catch (Exception e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        } finally {
            if (this.master != null) {
                this.master.disconnect();
            }
        }
        return this.errno;
    }
    /**
     * Метод синхронно записывает значение в регистр во внешний узел в указанный адрес. Метод используется в
     * целях отладки.
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param timeout : задержка по времени между итерациями по записи значений в регистры
     * @param itteration : кол-во итераций
     * @return errno : код ошибки
     */
    public int writeCycleHRSync(int idNodeSlave, int address, int timeout, int itteration) {
        ReportExcel reportExcel =
                new ReportExcel(String.format("writeCycleHR_%d_IDSlave", idNodeSlave));
        final Random random = new Random();
        this.timeoutSync=timeout;
        try {
            this.master.connect();
            while (itteration-- > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                float float32Bit = random.nextFloat();
                writeHRInSessionSync(idNodeSlave, address, this.timeoutSync,this.sleepSync, float32Bit);
                DataRegister dataRegister =
                        new DataRegister(address, currentTimeMillis, (double) float32Bit,String.format("%d",this.checkReadCounter));
                reportExcel.list.add(dataRegister);
                // TODO имитация работы Алгоритма Beremiz, убрать после отладки
                // writeRegistersInSession(master,idNodeSlave,address+TypesData.SIZE_32,calc(float32Bit));
                log.debug(dataRegister.toString());
            }
            this.errno = reportExcel.CreateReport();
            this.errMessage = reportExcel.getErrMessage();
        } catch (Exception e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        } finally {
            if (this.master != null) {
                this.master.disconnect();
            }
        }
        return this.errno;
    }

    /**
     * Метод читает значение из Slave-узла внутри сессии по указанному адресу из входного регистра.
     * <br>
     * Сохраняет в виде строки значение регистров в DEC, декодирует значение в float <br>
     * Строку со значением регистров можно прочитать через this.errMessage
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @return : float декодированное значение регистров
     */
    private short readIRInSession(int idNodeSlave, int address, int readCount) {
        short sValue = 0;
        try {
            InputRegister[] registersOut =
                    master.readInputRegisters(idNodeSlave, address, readCount);
            errMessage = registersOut.toString();
            WorkRegister registers = new WorkRegister();
            sValue = registers.registerToShort(registersOut);
            errMessage = String.format("sValue: %d", sValue);
            log.debug(errMessage);
        } catch (ModbusException e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        }
        return sValue;
    }

    /**
     * Метод читает значение из Slave-узла внутри сессии по указанному адресу. <br>
     * Сохраняет в виде строки значение регистров в DEC, декодирует значение в float <br>
     * Строку со значением регистров можно причитать через this.errMessage
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @return : float декодированное значение регистров
     */
    private float readHRInSession(int idNodeSlave, int address) {
        float fValue = 0.0F;
        try {
            Register[] registers =
                    this.master.readMultipleRegisters(idNodeSlave, address, TypesData.SIZE_32);
            byte[] appBytes = new byte[0];
            String strReport = new String("[");
            for (Register register : registers) {
                strReport += String.format("%d,", register.getValue());
                byte[] bytes = register.toBytes();
                appBytes = Utils.concatenate(appBytes, bytes);
            }
            strReport = strReport.substring(0, strReport.length() - 1);
            strReport += String.format("]");
            appBytes = Utils.reverseByteArray(appBytes);
            fValue = ModbusUtil.registersToFloat(appBytes);
            this.errMessage = String.format("%s", strReport);
            log.debug(errMessage);
        } catch (ModbusException e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        }
        return fValue;
    }

    /**
     * Синхронная чтение значения из указанного регистра внутри сессии, по следующему алгоритму:
     * <ol>
     *     <li>Читаем счетчик на запись расчетного значения Алгоритма (checkWriteCounter)</li>
     *     <li>Сравниваем с предыдущим (checkWriteCounter), c текущим</li>
     *     <li>Если текущее значение WRITE_COUNTER > checkWriteCounter, пишем новое значение</li>
     *     <li>Если текущее значение WRITE_COUNTER == checkWriteCounter, ждем timeout (ms)</li>
     *     <li>Задержака (sleep) в (ms) внутри цикла ожидания timeout </li>
     * </ol>
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param timeout : ожидание появления нового значение READ_COUNTER (ms)
     * @param sleep : Задержака (sleep) в (ms) внутри цикла ожидания timeout
     */
    private float readHRInSessionSync(int idNodeSlave, int address, int timeout, long sleep) {
        float fValue = 0.0F;
        try {
            short countWrite=readIRInSession(idNodeSlave,this.writeCountAddress,TypesData.SIZE_16);
            long currentTimeMillis = System.currentTimeMillis();
            while (countWrite==this.checkWriteCounter){
                Thread.sleep(sleep);
                countWrite=readIRInSession(idNodeSlave,this.writeCountAddress,TypesData.SIZE_16);
                if (System.currentTimeMillis()-currentTimeMillis > timeout)
                    break;
            }
            this.checkWriteCounter=countWrite;
            Register[] registers =
                    this.master.readMultipleRegisters(idNodeSlave, address, TypesData.SIZE_32);
            byte[] appBytes = new byte[0];
            for (Register register : registers) {
                byte[] bytes = register.toBytes();
                appBytes = Utils.concatenate(appBytes, bytes);
            }
            appBytes = Utils.reverseByteArray(appBytes);
            fValue = ModbusUtil.registersToFloat(appBytes);
            this.errMessage = String.format("%d",this.checkWriteCounter);
            log.debug(errMessage);
        } catch (ModbusException | InterruptedException e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        }
        return fValue;
    }

    /**
     * Метод в цикле производит чтение значений из конкретного регистра, указанного узла. <br>
     * Сохраняет значение в *.xlsx <br>
     * Применяется в отладочных целях.
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param itteration : кол-во иттераций
     */
    public int readCycleHR(int idNodeSlave, int address, int itteration) {
        ReportExcel reportExcel =
                new ReportExcel(String.format("readCycleHR_%d_IDSlave", idNodeSlave));
        try {
            this.master.connect();
            while (itteration-- > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                float float32Bit = readHRInSession(idNodeSlave, address);
                DataRegister dataRegister =
                        new DataRegister(
                                address, currentTimeMillis, (double) float32Bit, this.errMessage);
                reportExcel.list.add(dataRegister);
            }
            this.errno = reportExcel.CreateReport();
            this.errMessage = reportExcel.getErrMessage();
        } catch (Exception e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        return this.errno;
    }
    /**
     * Метод в цикле производит cинхронное чтение значений Алгоритма из конкретного регистра, указанного узла. <br>
     * Сохраняет значение в *.xlsx <br>
     * Применяется в отладочных целях.
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param itteration : кол-во иттераций
     */
    public int readCycleHRSync(int idNodeSlave, int address, int itteration) {
        ReportExcel reportExcel =
                new ReportExcel(String.format("readCycleHR_%d_IDSlave", idNodeSlave));
        try {
            this.master.connect();
            while (itteration-- > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                float float32Bit = readHRInSessionSync(idNodeSlave, address, this.timeoutSync,this.sleepSync);
                DataRegister dataRegister =
                        new DataRegister(
                                address, currentTimeMillis, (double) float32Bit, this.errMessage);
                reportExcel.list.add(dataRegister);
            }
            this.errno = reportExcel.CreateReport();
            this.errMessage = reportExcel.getErrMessage();
        } catch (Exception e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        return this.errno;
    }

    /**
     * Метод в цикле производит чтение значений из конкретного регистра (IR), указанного узла. <br>
     * Сохраняет значение в *.xlsx <br>
     *
     * <p>IR - записать значение нельзя он только для чтения. Применяется в отладочных целях.
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @param address : адрес регистра
     * @param sizeIR : размер входного регистра
     * @param timeout : задержка в мс в итерации
     * @param itteration : кол-во иттераций
     */
    public int readCycleIR(int idNodeSlave, int address, int sizeIR, int timeout, int itteration) {
        ReportExcel reportExcel =
                new ReportExcel(String.format("readCycleHR_%d_IDSlave", idNodeSlave));
        try {
            this.master.connect();
            while (itteration-- > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                float float32Bit = readIRInSession(idNodeSlave, address, sizeIR);
                DataRegister dataRegister =
                        new DataRegister(
                                address, currentTimeMillis, (double) float32Bit, this.errMessage);
                reportExcel.list.add(dataRegister);
                Thread.sleep(timeout);
            }
            this.errno = reportExcel.CreateReport();
            this.errMessage = reportExcel.getErrMessage();
        } catch (Exception e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                master.disconnect();
            }
        }
        return this.errno;
    }

    /**
     * Метод читает содержимое Slave-узла, регистры которого 32-bits
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @return : код ошибки
     */
    public int readMultiHR(int idNodeSlave) {
        ReportExcel reportExcel = new ReportExcel(String.format("MasterSlave_%d", idNodeSlave));
        try {
            this.master.connect();
            for (int i = 0; i < TypesData.MAX_COUNT_REGISTER; i += TypesData.SIZE_32) {
                long currentTimeMillis = System.currentTimeMillis();
                float float32Bit = readHRInSession(idNodeSlave, i);
                DataRegister dataRegister =
                        new DataRegister(
                                i, currentTimeMillis, (double) float32Bit, this.errMessage);
                reportExcel.list.add(dataRegister);
            }
            this.errno = reportExcel.CreateReport();
            this.errMessage = reportExcel.getErrMessage();
        } catch (Exception e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(this.errMessage);
        } finally {
            if (this.master != null) {
                this.master.disconnect();
            }
        }
        return this.errno;
    }

    /**
     * Метод читает, затем записывает новое рассчитанное значение в регистр во внешний узел
     *
     * @param idNodeSlave : идентификатор Slave-узла
     * @return : код ошибки
     */
    public int writeMultiHR(int idNodeSlave) {
        ReportExcel reportExcel = new ReportExcel(String.format("MasterSlave_%d", idNodeSlave));
        final Random random = new Random();
        try {
            this.master.connect();
            for (int i = 0; i < TypesData.MAX_COUNT_REGISTER * 2; i += TypesData.SIZE_32) {
                long currentTimeMillis = System.currentTimeMillis();
                float float32Bit = random.nextFloat();
                WorkRegister registers = new WorkRegister();
                master.writeMultipleRegisters(
                        idNodeSlave, i + 1, registers.floatToRegister(float32Bit));
                DataRegister dataRegister =
                        new DataRegister(
                                i,
                                currentTimeMillis,
                                (double) float32Bit,
                                registers.getErrMessage());
                reportExcel.list.add(dataRegister);
            }
            this.errno = reportExcel.CreateReport();
            this.errMessage = reportExcel.getErrMessage();
        } catch (Exception e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        } finally {
            if (master != null) {
                this.master.disconnect();
            }
        }
        return this.errno;
    }
    /**
     * Метод читает содержимое регистра хранящего 16-bit значение, инкремирует его и записывает
     * обратно.
     *
     * @param idSlave : идентификатор Slave-узла
     * @param address : адрес регистра, где хранится значение счетчика
     * @return : short (новое значение счетчика)
     */
    public short addHRCounter(int idSlave, int address) {
        short sValue = 0;
        try {
            InputRegister[] registersOut =
                    this.master.readMultipleRegisters(idSlave, address, TypesData.SIZE_16);
            WorkRegister registers = new WorkRegister();
            sValue = registers.registerToShort(registersOut);
            sValue++;
            this.master.writeSingleRegister(idSlave, address, registers.shortToRegister(sValue));
            this.errMessage = String.format("sValue: %d", sValue);
            log.debug(this.errMessage);
        } catch (ModbusException e) {
            this.errno = ErrorCode.EXCEPTIONS;
            this.errMessage = String.format("Error:%d;Error:%s",this.errno,e.getMessage());
            log.error(errMessage);
        }
        return sValue;
    }
}
