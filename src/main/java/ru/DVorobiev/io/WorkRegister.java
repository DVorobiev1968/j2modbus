package ru.DVorobiev.io;

import com.ghgande.j2mod.modbus.procimg.InputRegister;
import com.ghgande.j2mod.modbus.procimg.Register;
import com.ghgande.j2mod.modbus.procimg.SynchronizedAbstractRegister;
import com.ghgande.j2mod.modbus.util.ModbusUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import ru.DVorobiev.TypesData;
import ru.DVorobiev.utils.Utils;

@Slf4j
@Getter
public class WorkRegister extends SynchronizedAbstractRegister implements Register {
    private String errMessage;

    /**
     * Constructs a new <tt>WorkRegister</tt> instance.
     *
     * @param b1 the first (hi) byte of the word.
     * @param b2 the second (low) byte of the word.
     */
    public WorkRegister(byte b1, byte b2) {
        register[0] = b1;
        register[1] = b2;
    }

    /**
     * Constructs a new <tt>WorkRegister</tt> instance with the given value.
     *
     * @param value the value of this <tt>WorkRegister</tt> as <tt>int</tt>.
     */
    public WorkRegister(int value) {
        setValue(value);
    }

    /**
     * Constructs a new <tt>WorkRegister</tt> instance. It's state will be invalid.
     *
     * <p>Attempting to access this register will result in an IllegalAddressException(). It may be
     * used to create "holes" in a Modbus register map.
     */
    public WorkRegister() {
        register = null;
        errMessage = new String();
    }

    public String toString() {
        if (register == null) {
            return "invalid";
        }
        return getValue() + "";
    }

    public Register shortToRegister(short sValue) {
        byte[] bytes = ModbusUtil.shortToRegister(sValue);
        //        bytes= Utils.reverseByteArray(bytes);
        Register register = null;
        for (int iByte = 0; iByte < TypesData.SIZE_16; iByte++) {
            register = new WorkRegister(bytes[iByte], bytes[iByte + 1]);
        }
        return register;
    }

    public short registerToShort(InputRegister[] registers) {
        byte[] appBytes = new byte[0];
        for (InputRegister register : registers) {
            byte[] bytes = register.toBytes();
            appBytes = Utils.concatenate(appBytes, bytes);
        }
        //        appBytes=Utils.reverseByteArray(appBytes);
        return ModbusUtil.registerToShort(appBytes);
    }

    public int registerToInt(InputRegister[] registers) {
        byte[] appBytes = new byte[0];
        for (InputRegister register : registers) {
            byte[] bytes = register.toBytes();
            appBytes = Utils.concatenate(appBytes, bytes);
        }
        appBytes = Utils.reverseByteArray(appBytes);
        return ModbusUtil.registersToInt(appBytes);
    }

    public float registerToFloat(InputRegister[] registers) {
        byte[] appBytes = new byte[0];
        for (InputRegister register : registers) {
            byte[] bytes = register.toBytes();
            appBytes = Utils.concatenate(appBytes, bytes);
        }
        appBytes = Utils.reverseByteArray(appBytes);
        return ModbusUtil.registersToFloat(appBytes);
    }

    public Register[] floatToRegister(float value) {
        int index = 0;
        byte[] bytes = ModbusUtil.floatToRegisters(value);
        bytes = Utils.reverseByteArray(bytes);
        String strRegister = new String("[");
        Register[] registers = new Register[TypesData.SIZE_32];
        for (int iByte = 0; iByte < TypesData.SIZE_WORD; iByte += 2) {
            registers[index] = new WorkRegister(bytes[index * iByte], bytes[index * iByte + 1]);
            strRegister += String.format("%s,", registers[index].toString());
            index++;
        }
        strRegister = strRegister.substring(0, strRegister.length() - 1);
        strRegister += String.format("]");
        errMessage += String.format("%4.10f;%s", value, strRegister);
        return registers;
    }

    public String floatToRegisterToString(float value) {
        int index = 0;
        byte[] bytes = ModbusUtil.floatToRegisters(value);
        bytes = Utils.reverseByteArray(bytes);
        String strRegister = new String("[");
        Register[] registers = new Register[TypesData.SIZE_32];
        for (int iByte = 0; iByte < TypesData.SIZE_WORD; iByte += 2) {
            registers[index] = new WorkRegister(bytes[index * iByte], bytes[index * iByte + 1]);
            strRegister += String.format("%s,", registers[index].toString());
            index++;
        }
        strRegister = strRegister.substring(0, strRegister.length() - 1);
        strRegister += String.format("]");
        errMessage += String.format("%4.10f;%s", value, strRegister);
        return strRegister;
    }
}
