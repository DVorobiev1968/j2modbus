package ru.DVorobiev.io;

import java.io.DataOutput;
import java.io.IOException;

public class DataOutputForRegister implements DataOutput {
    @Override
    public void write(int i) throws IOException {}

    @Override
    public void write(byte[] bytes) throws IOException {}

    @Override
    public void write(byte[] bytes, int i, int i1) throws IOException {}

    @Override
    public void writeBoolean(boolean b) throws IOException {}

    @Override
    public void writeByte(int i) throws IOException {}

    @Override
    public void writeShort(int i) throws IOException {}

    @Override
    public void writeChar(int i) throws IOException {}

    @Override
    public void writeInt(int i) throws IOException {}

    @Override
    public void writeLong(long l) throws IOException {}

    @Override
    public void writeFloat(float v) throws IOException {}

    @Override
    public void writeDouble(double v) throws IOException {}

    @Override
    public void writeBytes(String s) throws IOException {}

    @Override
    public void writeChars(String s) throws IOException {}

    @Override
    public void writeUTF(String s) throws IOException {}
}
