package ru.DVorobiev;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Settings {
    /** имя хоста */
    public static final String HOST = "localhost";
    /** номер порта */
    public static final int PORT = 5020;
    /** время синхронизации ожидания счетчика READ_COUNTER */
    public static final int TIMEOUT_READ_COUNTER_DEFAULT=20;
    /** засыпание в цикле синхронизации ожидания счетчика READ_COUNTER */
    public static final int SLEEP_READ_COUNTER_DEFAULT=1;
}
