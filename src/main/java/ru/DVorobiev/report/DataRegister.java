package ru.DVorobiev.report;

import java.nio.charset.StandardCharsets;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import lombok.Data;
import lombok.Getter;
import ru.DVorobiev.io.WorkRegister;

@Data
@Getter
public class DataRegister {
    /** Метка времени дата */
    private Date date;
    /** Метка времени */
    private long time;
    /** Адрес регистра */
    private int addr;
    /** Значение регистра формате Double */
    private Double value;
    /** Значение регистра формате String (word/dword) */
    private String sValue;

    /**
     * Дату присваеваем автоматически
     *
     * @param addr: идентификатор узла
     * @param time : метка времени
     * @param value: значение от процесса
     */
    public DataRegister(int addr, long time, Double value, String sValue) {
        this.addr = addr;
        this.date = new Date();
        this.time = time;
        this.value = value;
        this.sValue = sValue;
    }

    public DataRegister(int addr, long time, Double value) {
        this.addr = addr;
        this.date = new Date();
        this.time = time;
        this.value = value;
        WorkRegister workRegister = new WorkRegister();
        this.sValue = workRegister.floatToRegisterToString(value.floatValue());
    }

    public DataRegister(int addr, long time, Double value, byte[] bytes) {
        this.addr = addr;
        this.date = new Date();
        this.time = time;
        this.value = value;
        this.sValue = new String(bytes, StandardCharsets.UTF_8);
    }

    /**
     * метод который работает по умолчанию, автоматически присваивает текущую дату
     *
     * @return String: строка дата время с учетом Locale
     */
    public String getDateString() {
        String dateStr;
        Locale locale = new Locale("ru", "RU");
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
        dateFormatSymbols.setWeekdays(
                new String[] {
                    "Не используется",
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница",
                    "Суббота",
                    "Воскресенье"
                });

        String pattern = "dd/MM/yyyy HH:mm:ss.SSS";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    /**
     * метод который преобразовывает дату и время с учетом Locale
     *
     * @param date дата
     * @return String: строка дата время с учетом Locale
     */
    public String getDateString(Date date) {
        String dateStr, timeStr;
        String pattern;

        SimpleDateFormat simpleDateFormat;
        Locale locale = new Locale("ru", "RU");
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
        dateFormatSymbols.setWeekdays(
                new String[] {
                    "Не используется",
                    "Понедельник",
                    "Вторник",
                    "Среда",
                    "Четверг",
                    "Пятница",
                    "Суббота",
                    "Воскресенье"
                });

        pattern = "dd/MM/yyyy";
        simpleDateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        dateStr = simpleDateFormat.format(date);
        this.setDate(new Date(dateStr));

        pattern = "dd/MM/yyyy HH:mm:ss.SSS";
        simpleDateFormat = new SimpleDateFormat(pattern, dateFormatSymbols);
        dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    @Override
    public String toString() {
        return String.format(
                "%s;%d;%4.15f;%s",
                this.getDateString(), this.getAddr(), this.getValue(), this.getSValue());
    }
}
