package ru.DVorobiev.utils;

import com.ghgande.j2mod.modbus.procimg.Register;
import com.ghgande.j2mod.modbus.util.ModbusUtil;
import java.lang.reflect.Array;
import lombok.experimental.UtilityClass;
import ru.DVorobiev.TypesData;
import ru.DVorobiev.io.WorkRegister;

@UtilityClass
public class Utils {
    public static String errMessage = new String();

    public static <T> T concatenate(T a, T b) {
        if (!a.getClass().isArray() || !b.getClass().isArray()) {
            throw new IllegalArgumentException();
        }

        Class<?> resCompType;
        Class<?> aCompType = a.getClass().getComponentType();
        Class<?> bCompType = b.getClass().getComponentType();

        if (aCompType.isAssignableFrom(bCompType)) {
            resCompType = aCompType;
        } else if (bCompType.isAssignableFrom(aCompType)) {
            resCompType = bCompType;
        } else {
            throw new IllegalArgumentException();
        }

        int aLen = Array.getLength(a);
        int bLen = Array.getLength(b);

        @SuppressWarnings("unchecked")
        T result = (T) Array.newInstance(resCompType, aLen + bLen);
        System.arraycopy(a, 0, result, 0, aLen);
        System.arraycopy(b, 0, result, aLen, bLen);
        return result;
    }

    private void swapByte(byte[] arr, int a, int b) {
        byte temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }

    public byte[] reverseByteArray(byte[] arr) {
        int size = arr.length - 1;
        for (int i = 0; i < size; i++) {
            swapByte(arr, i, size--);
        }
        return arr;
    }

    public static byte[] floatToByteArray(float value) {
        int intBits = Float.floatToIntBits(value);
        return new byte[] {
            (byte) (intBits >> 24), (byte) (intBits >> 16), (byte) (intBits >> 8), (byte) (intBits)
        };
    }

    public static float byteArrayToFloat(byte[] bytes) {
        int intBits =
                bytes[0] << 24
                        | (bytes[1] & 0xFF) << 16
                        | (bytes[2] & 0xFF) << 8
                        | (bytes[3] & 0xFF);
        return Float.intBitsToFloat(intBits);
    }

    public static Register[] floatToRegister(float value) {
        int index = 0;
        byte[] bytes = ModbusUtil.floatToRegisters(value);
        bytes = Utils.reverseByteArray(bytes);
        String strRegister = new String("[");
        Register[] registers = new Register[TypesData.SIZE_32];
        for (int iByte = 0; iByte < TypesData.SIZE_WORD; iByte += 2) {
            registers[index] = new WorkRegister(bytes[index * iByte], bytes[index * iByte + 1]);
            strRegister += String.format("%s,", registers[index].toString());
            index++;
        }
        strRegister = strRegister.substring(0, strRegister.length() - 1);
        strRegister += String.format("]");
        errMessage += String.format("%4.10f;%s", value, strRegister);
        return registers;
    }
}
