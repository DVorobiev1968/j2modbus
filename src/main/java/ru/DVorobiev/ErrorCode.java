package ru.DVorobiev;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ErrorCode {
    // коды ошибок уровня работы технологического сервера
    public static final int OK = 80;

    public static final int SEARCH_FAIL = 78;
    public static final int SEARCH_OK = 79;
    public static final int ERR_CLOSE_CONNECT = 95;
    public static final int UNKNOW_HOST = 96;
    public static final int RESET_HOST = 97;
    public static final int B_MESSAGE_EMPTY = 98;
    public static final int ERR = 99;
    public static final int SYNTAX_ERR = 101;
    public static final int ERR_FUNC = -1;
    public static final int UPDATE_OK = 64;
    public static final int READ_SOCKET_FAIL = 68;
    public static final int READ_SOCKET_OK = 69;
    public static final int CODE_ALGORITM_OPERATION = 50;
    public static final int SET_ALGORITM_VAL_OK = 52;
    public static final int SET_ALGORITM_VAL_FAIL = 51;
    public static final int SET_ALGORITM_WAIT = 55;

    // коды ошибок уровня Client API
    public static final int ERROR_API = 700;

    public static final int API_OK = ERROR_API + 1;
    public static final int CSV_OK = ERROR_API + 2;
    public static final int ERROR_FIND_NODES_EXEPTION = ERROR_API + 30;
    public static final int ERROR_MULTI_NODES = ERROR_API + 10;
    public static final int ERROR_MULTI_NODES_EXCEPTION = ERROR_API + 11;
    public static final int ERROR_MULTI_VALUES = ERROR_API + 20;
    public static final int ERROR_MULTI_VALUES_EXCEPTION = ERROR_API + 21;
    public static final int ERROR_CSV_NOT_FOUND = ERROR_API + 12;
    public static final int ERROR_CSV_IO = ERROR_API + 13;
    public static final int ERROR_XLS_IO = ERROR_API + 14;
    public static final int NODES_EMPTY = -(ERROR_API + 14);
    public static final int XLS_OK = ERROR_API + 3;
    public static final int XLS_ERR = ERROR_API - 1;
    public static final int XLS_EMPTY = ERROR_API - 2;
    public static final int LIST_NODES_SAVE_IN_SERVER = ERROR_API + 31;

    // коды ошибок связанные с исключениями
    public static final int EXCEPTIONS = 800;
    public static final int EXEPTION_BUFFER_FROM_SERVER = EXCEPTIONS + 1;
    public static final int EXEPTION_PARSER = EXCEPTIONS + 2;
    public static final int INVALID_NODE_ID = EXCEPTIONS + 3;
    public static final int INVALID_OBJECT_ID = EXCEPTIONS + 4;
    public static final int INVALID_SUBOBJECT_ID = EXCEPTIONS + 5;
    public static final int NOT_SUPPORT_TYPE_DATA = EXCEPTIONS + 6;
    public static final int INVALID_TYPE_DATA = EXCEPTIONS + 7;
}
